/**
 * Specification for the Configurable container view.
 * @author Gavin Harris
 * @date 08/2014
 */
describe('ADT.view.MainViewport', function(){

	var view;

	beforeEach(function() {
		Core.InactivityManager.session.applicationName = 'PatientViewBase';
		view = view || Ext.create('ADT.view.MainViewport', {
			childComponents: [{
				name: 'ConfigurableContainer',
				skipController: true,
				layout: 'hbox'
			}]
		});
	});

	describe('initComponent', function(){
		it('applies the layout configuration', function(){
			expect(view.items.length).toBe(1);
		});
	});
});