/**
 * Specification for the ADT.controller.ApplicationController controller.
 * @see ADT/controller/ApplicationController.js
 * @author Gavin Harris.
 */
/*jslint jasmine: true*/
describe('ADT.controller.ApplicationController', function() {
	'use strict';
	var controller;

	beforeEach(function() {
		controller = controller || Ext.create('ADT.controller.ApplicationController');
	});

	describe('changeTab', function() {
		var fireSpy;

		beforeEach(function() {
			fireSpy = spyOn(Core.EventBus, 'fireEvent');
			controller.init();
			controller.changeTab('tabpanel', {xtype: 'new', title: 'Title'}, {xtype: 'old'});
		});

		it('fires the display event for the adt namespace', function() {
			expect(fireSpy).toHaveBeenCalledWith('adt.new.display');
			expect(fireSpy).toHaveBeenCalledWith('adt.Title.display');
		});
	});
});
