/**
 * @module Viewport
 * @memberof PatientViewBase.view
 * @author Gavin Harris
 * @date 08/2014
 * @augments PatientViewBase.view.module:MainViewport
 */
Ext.define('ADT.view.Viewport', {
	extend: 'ADT.view.MainViewport',
	renderTo: Ext.getBody(),
	rtl: Ext.Loader.locale.rtl,
	requires: [
		'Ext.direct.DeferredProvider',
		'Ext.ux.panel.header.ExtraIcons'
	]
});
