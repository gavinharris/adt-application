/**
 * Main viewport for the Viper360 PatientView.
 * @module MainViewport
 * @memberof PatientViewBase.view
 * @author Gavin Harris
 * @date 08/2014
 * @augments Ext.container.Viewport
 */
Ext.define('ADT.view.MainViewport', {
	extend: 'Ext.container.Viewport',
	requires: [ 'Ext.layout.container.Card' ],
	mixins: ['Core.view.ConfigurableComponent'],

	id: 'mainvp',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	childComponents: [],

	/**
	 * Initialise the view.
	 */
	initComponent: function() {
		Ext.applyIf(this, {
			items: this.createChildXTypes(this.childComponents)
		});
		this.callParent(arguments);
	}
});
