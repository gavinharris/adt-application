/**
 * Controller to manage the display of pages in the application.
 * @module ApplicationController
 * @memberof PatientViewBase.controller
 * @author Gavin Harris
 * @date 02/2015
 * @augments Core.controller.GenericController
 */
Ext.define('ADT.controller.ApplicationController', {
	extend : 'Core.controller.ApplicationController',

	refs: [{
		ref: 'cards',
		selector: 'viewport > configurablecontainer'
	}],

	namespace: 'adt',

	init: function() {
		this.callParent(arguments);
	}
});
