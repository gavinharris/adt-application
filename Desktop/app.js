/**
 * Application launcher for the PatientViewBase application.
 * @author Gavin Harris
 * @date 08/2014
 */
/*globals approot*/
var config = Ext.Loader.getConfig();
Ext.Loader.setConfig(config);

Ext.require('Core.ApiManager');
Ext.require('Core.Session');
Ext.require('Core.EventBus');
Ext.require('PatientViewBase.ApplicationStarter');
Ext.require('Ext.layout.container.Fit');
Ext.require('Ext.container.Viewport');

Ext.onReady(function() {

	// Setup Logger for all classes (with mixin).
	Ext.create('Core.Logger');
	Core.Logger.setDebug(true);
	var apis = Ext.create('Core.ApiManager', { apis : [] }),
		createSession = function() {
			Ext.create('Core.Session', {
				applicationName: 'ADT',
				ApiManager : apis,
				launcher : PatientViewBase.ApplicationStarter.launchRoleApplication
			});
		};

	createSession();

});
