/**
 * Patient View screen labels in GB English.
 * @author Gavin Harris
 * @date 08/2014.
 */
Ext.define('ADT.Labels', {
	singleton: true,

	toolbar : {
		logo : {
			tooltip : 'Return to the Dashboard'
		}
	}
});
